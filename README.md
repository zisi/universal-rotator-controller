## Universal Rotator Controller
A board that used to control either two 2-phase (presumably
quadrature), capacitor-run, AC induction motors or two DC motors.
The specification of motor drives and of specific peripherals is based on this [rotator analysis](https://wiki.satnogs.org/Review_of_Commercial_Rotators).
A [discussion](https://community.libre.space/t/universal-rotator-controller/4334) related to the control of AC motors.

Stable versions are tagging. More details related to tagging TBD.

## Contribute
This is a KiCAD project thus merging can be tricky.
Coordinate with project engineers before starting any changes and take look at issues.

### EDA
KiCAD 5.1
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

#### Schematic
Changes in manufacturer part numbers field, footprints and designators can be merged most of the times.
Any part change, change in placement, wire placement etc cannot be merged at this point.

One exception is Hierarchical sheets provided that:

* A single person is working on a Hierarchical sheet
* Annotation is using sheet number
* Global nets are respected
* Changes in Hierarchical labels are coordinated

Design rules:

* Grid size: 1.27mm
* Filed in BoM are setting from Eeschema, Preference->Field Name Templates,
  * Mfr.: Contains the name of manufacturer.
  * Part Number: Contains the part number of manufacturer.
  * Description: Describe the type of part, for example Capacitor, 2.2uF-16V-X5R-10%
  * Variant: It refers to different configurations of PCB.
  * Comment: It contains comments for example in assembly process.

#### PCB
At this point any merging on PCB files must be avoided.
Also for the PCB (*.kicad_pcb) is used [LFS](https://docs.gitlab.com/ee/topics/git/lfs/) to prevent
a large size repository and to use the feature of LFS lock. The LFS lock protects the developers
to have conflicts in PCB that aren't merged. For that reason use:

```
git lfs clone
```

to clone the repository.

The design rules are imported from [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) fabrication template:

* Text & Graphics default properties
* Design Rules
* Solder Mask/Past defaults

### Qucs
For simulation of circuits are used [Qucs 0.0.20](https://github.com/Qucs/qucs).
To open a QUCS project, Project->Open Project...

## License
Licensed under the [CERN OHLv1.2](LICENSE)
