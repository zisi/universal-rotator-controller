<Qucs Schematic 0.0.20>
<Properties>
  <View=-90,-420,1160,1460,0.741253,0,0>
  <Grid=10,10,1>
  <DataSet=spwm-filter-bode.dat>
  <DataDisplay=spwm-filter-bode.dpl>
  <OpenDisplay=1>
  <Script=spwm-filter-bode.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Vac V1 1 60 190 -63 -26 1 1 "1 V" 1 "1 MHz" 0 "0" 0 "0" 0>
  <GND *1 5 60 350 0 0 0 0>
  <Eqn Bode 1 670 40 -31 19 0 0 "phase=phase(vout.v/vin.v)" 1 "gain=dB(vout.v/vin.v)" 1 "yes" 0>
  <.AC AC1 1 450 30 0 47 0 0 "lin" 1 "1" 1 "50 k" 1 "1000" 1 "no" 0>
  <GND *2 5 240 350 0 0 0 0>
  <L L2 1 240 40 10 -26 0 1 "L_filt" 1 "" 0>
  <C C1 1 190 190 -83 -26 1 1 "C_sim" 1 "" 0 "neutral" 0>
  <.SW SW1 1 440 200 0 60 0 0 "AC1" 1 "lin" 1 "R1" 1 "1 Ohm" 1 "6 Ohm" 1 "6" 1>
  <C C3 1 380 180 -83 -26 0 3 "100uF" 1 "" 0 "neutral" 0>
  <R R3 1 260 180 -56 -26 0 3 "R1" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <R R2 1 320 260 -26 -53 0 2 "R1" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <Eqn Filter1 1 690 180 -31 19 0 0 "C_filt=470*1e-9" 1 "L_filt=168*1e-6" 1 "C_sim=2*C_filt" 1 "yes" 0>
</Components>
<Wires>
  <60 220 60 350 "" 0 0 0 "">
  <240 300 240 350 "" 0 0 0 "">
  <240 300 280 300 "" 0 0 0 "">
  <60 0 60 160 "" 0 0 0 "">
  <60 0 240 0 "" 0 0 0 "">
  <240 0 240 10 "" 0 0 0 "">
  <240 70 240 80 "" 0 0 0 "">
  <240 80 280 80 "" 0 0 0 "">
  <190 80 240 80 "" 0 0 0 "">
  <190 80 190 160 "" 0 0 0 "">
  <190 300 240 300 "" 0 0 0 "">
  <190 220 190 300 "" 0 0 0 "">
  <350 260 380 260 "" 0 0 0 "">
  <380 210 380 260 "" 0 0 0 "">
  <380 120 380 150 "" 0 0 0 "">
  <260 260 280 260 "" 0 0 0 "">
  <260 210 260 260 "" 0 0 0 "">
  <260 120 280 120 "" 0 0 0 "">
  <260 120 260 150 "" 0 0 0 "">
  <280 260 290 260 "" 0 0 0 "">
  <280 260 280 300 "" 0 0 0 "">
  <280 120 380 120 "" 0 0 0 "">
  <280 80 280 120 "vout" 330 60 20 "">
  <240 0 240 0 "vin" 280 -40 0 "">
</Wires>
<Diagrams>
  <Rect 30 941 749 511 3 #c0c0c0 1 10 1 10 1 100000 1 -30 1 1 1 -1 0.2 1 315 0 225 "" "" "" "">
	<"gain" #0000ff 2 3 0 0 0>
	  <Mkr 101.098/2 132 -334 3 0 0>
	  <Mkr 601.589/2 269 -557 3 0 0>
	  <Mkr 10060.9/2 322 -181 3 0 0>
	  <Mkr 1252.23/2 774 -579 3 0 0>
	  <Mkr 17718.4/2 296 -92 3 0 0>
	  <Mkr 29479.9/2 890 -115 3 0 0>
	  <Mkr 1052.03/2 543 -599 3 0 0>
	  <Mkr 3354.29/4 948 -526 3 0 0>
	  <Mkr 27728.2/6 776 -287 3 0 0>
	  <Mkr 3554.48/2 592 -436 3 0 0>
	  <Mkr 3654.58/3 594 -456 3 0 0>
  </Rect>
</Diagrams>
<Paintings>
</Paintings>
